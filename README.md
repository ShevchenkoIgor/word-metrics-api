**Task:**

* The application should accept requests with one parameter, which is a string representing one English word. 

* For this English word the application should return an output composed of 2 integers: 

  - first one is how often it was called with the same value of this parameter
  
  - the other one is how many times this word occurs in a set of text files that the application has as its internal resources
  
  
**Checkout and build**

* Checkout sources and prepare required text files as internal resources. 

Open resources->strore and add all files. It should looks like this https://snag.gy/284mE5.jpg 

* Build application:

    - **_cd_** to project directory.
    
    - run **_mvn clean install_** to build jar file.
    
    
**Run and Test Spring boot application**

 - run **_java -jar {jar_path}/word-metrics-api.jar_**
 
 - navigate browser to _http://localhost:8081/api/metric/test_word_ . Application has following endpoint _GET /api/metric/{word}_
   
   
Application can be tested with tests:

- prepare test resource folder with required files like https://snag.gy/J14TCL.jpg

- run tests
