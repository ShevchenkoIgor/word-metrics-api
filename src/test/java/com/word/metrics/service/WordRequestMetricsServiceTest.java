package com.word.metrics.service;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WordRequestMetricsServiceTest {

    private String testWord;

    @Autowired
    private WordRequestMetricsService wordRequestMetricsService;

    @Before
    public void setup() {
        testWord = "Test";
    }

    /**
     * Counting number of requests for test word
     * @result Count number should be returned without any errors.
     */
    @Test()
    public void firstRequestTest(){
        int count = wordRequestMetricsService.getRequestMetric(testWord);
        assertEquals(1, count);
    }

    /**
     * Counting number of requests for test word
     * @result Count number should be returned without any errors.
     */
    @Test()
    public void secondRequestTest(){
        int count = wordRequestMetricsService.getRequestMetric(testWord);
        assertEquals(2, count);
    }

    @Configuration
    static class WordRequestMetricsServiceTestContextConfiguration {

        @Bean
        public WordRequestMetricsService wordRequestMetricsService() {
            return new WordRequestMetricsService();
        }

    }

}
