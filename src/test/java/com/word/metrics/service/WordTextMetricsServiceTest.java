package com.word.metrics.service;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WordTextMetricsServiceTest {

    private String testWord;
    @Autowired
    private WordTextMetricsService wordTextMetricsService;
    @Autowired
    private PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver;

    @Before
    public void setup() throws IOException {
        testWord = "Test_Word";

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = resolver.getResources("classpath:store/*");

        Mockito.when(pathMatchingResourcePatternResolver.getResources(Mockito.anyString())).thenReturn(resources);
    }

    @After
    public void verify() throws IOException {
        Mockito.reset(pathMatchingResourcePatternResolver);
    }

    /**
     * Counting word in text files
     * @result Count number should be returned without any errors.
     */
    @Test()
    public void firstRequestTest() throws IOException {
        int count = wordTextMetricsService.getTextMetric(testWord);
        Mockito.verify(pathMatchingResourcePatternResolver, VerificationModeFactory.times(1)).getResources(Mockito.anyString());
        assertEquals(5, count);
    }

    /**
     * Count number should be taken from cash. There is no interaction with getResources method.
     * @result Count number should be returned without any errors.
     */
    @Test()
    public void secondRequestTest() throws IOException {
        int count = wordTextMetricsService.getTextMetric(testWord);
        Mockito.verify(pathMatchingResourcePatternResolver, VerificationModeFactory.times(0)).getResources(Mockito.anyString());
        assertEquals(5, count);
    }

    @Configuration
    static class WordTextMetricsServiceTestContextConfiguration {

        @Bean
        public WordTextMetricsService wordTextMetricsService() {
            return new WordTextMetricsService(pathMatchingResourcePatternResolver());
        }

        @Bean
        public PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver() {
            return Mockito.mock(PathMatchingResourcePatternResolver.class);
        }
    }
}
