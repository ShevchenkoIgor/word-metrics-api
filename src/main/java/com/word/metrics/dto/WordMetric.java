package com.word.metrics.dto;

/**
 * DTO class for response
 *
 * @author Shevchenko Igor
 * @since 2016-07-13
 */
public class WordMetric {
    
    private int wordRequestCount;
    private int wordTextCount;

    public int getWordRequestCount() {
        return wordRequestCount;
    }

    public void setWordRequestCount(int wordRequestCount) {
        this.wordRequestCount = wordRequestCount;
    }

    public int getWordTextCount() {
        return wordTextCount;
    }

    public void setWordTextCount(int wordTextCount) {
        this.wordTextCount = wordTextCount;
    }
}
