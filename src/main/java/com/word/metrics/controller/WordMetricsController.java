package com.word.metrics.controller;

import com.word.metrics.dto.WordMetric;
import com.word.metrics.service.WordRequestMetricsService;
import com.word.metrics.service.WordTextMetricsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Controller class with Rest endpoint
 *
 * @author Shevchenko Igor
 * @since 2016-07-13
 */
@RequestMapping("/api/metric")
@RestController
public class WordMetricsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WordMetricsController.class);

    private final WordRequestMetricsService wordRequestMetricsService;
    private final WordTextMetricsService wordTextMetricsService;

    @Autowired
    public WordMetricsController(WordRequestMetricsService wordRequestMetricsService, WordTextMetricsService wordTextMetricsService) {
        this.wordRequestMetricsService = wordRequestMetricsService;
        this.wordTextMetricsService = wordTextMetricsService;
    }

    @RequestMapping(value = "/{word}", method = GET)
    public ResponseEntity<WordMetric> getUser(@PathVariable String word) throws Exception {
        LOGGER.info("Getting metrics for word={}", word);
        WordMetric wordMetric = new WordMetric();
        wordMetric.setWordRequestCount(wordRequestMetricsService.getRequestMetric(word));
        wordMetric.setWordTextCount(wordTextMetricsService.getTextMetric(word));

        return new ResponseEntity<>(wordMetric, HttpStatus.OK);
    }
}
