package com.word.metrics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Main class to start words metrics application
 *
 * @author  Shevchenko Igor
 * @since   2016-07-13
 */
@SpringBootApplication
public class WordMetricsApplication {

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(WordMetricsApplication.class, args);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods("GET", "POST", "PUT", "DELETE")
                        .allowCredentials(false).maxAge(7200);
            }
        };
    }

    @Bean
    public PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver(){
        return new PathMatchingResourcePatternResolver();
    }
}