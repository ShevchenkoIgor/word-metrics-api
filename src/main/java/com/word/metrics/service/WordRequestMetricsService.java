package com.word.metrics.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Service class to count words from request parameter
 *
 * @author Shevchenko Igor
 * @since 2016-07-13
 */
@Service
public class WordRequestMetricsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WordRequestMetricsService.class);

    private ConcurrentMap<String, Integer> wordMetrics;

    public WordRequestMetricsService() {
        wordMetrics = new ConcurrentHashMap<>();
    }

    /**
     * Method which counts how often word was called with the same value
     * in request
     *
     * @param word This is the string value for counting
     * @return int This returns count number
     */
    public int getRequestMetric(String word) {
        LOGGER.info("Counting '{}' frequency from requests", word);
        Integer wordCount = wordMetrics.get(word);

        if (wordCount == null) {
            wordCount = 1;
        } else {
            wordCount++;
        }

        wordMetrics.put(word, wordCount);
        return wordCount;
    }
}
