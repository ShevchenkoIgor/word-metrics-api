package com.word.metrics.service;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static java.lang.Math.toIntExact;

/**
 * Service class to count words in text files
 *
 * @author Shevchenko Igor
 * @since 2016-07-13
 */
@Service
public class WordTextMetricsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WordTextMetricsService.class);

    private ConcurrentMap<String, Integer> wordMetric;

    private PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver;

    @Value("${resource.path}")
    private String resourcePath;

    @Autowired
    public WordTextMetricsService(PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver) {
        this.pathMatchingResourcePatternResolver = pathMatchingResourcePatternResolver;
        this.wordMetric = new ConcurrentHashMap<>();
    }

    /**
     * Method which counts how many times this word occurs in a set of text files that
     * the application has as its internal resources.
     *
     * @param word This is the string value for counting
     * @return int This returns count number
     */
    public int getTextMetric(String word) throws IOException {

        Integer wordCount = wordMetric.get(word);

        if (wordCount != null) {
            return wordCount;
        } else {
            wordCount = 0;
        }

        for (InputStream file : initFiles()) {
            LOGGER.info("Counting '{}' frequency in file ...", word);

            LineIterator it = IOUtils.lineIterator(file, "UTF-8");
            try {
                while (it.hasNext()) {
                    String line = it.nextLine();
                    wordCount += processLine(word, line);
                }
            } finally {
                LineIterator.closeQuietly(it);
                LOGGER.info("Counting '{}' frequency in file was finished successfully", word);
            }
        }

        wordMetric.put(word, wordCount);
        return wordCount;
    }

    private List<InputStream> initFiles() {

        List<InputStream> filesInputStream = new ArrayList<>();

        try {
            LOGGER.info("Trying to load text files for word counting...");

            Resource[] resources = pathMatchingResourcePatternResolver.getResources(resourcePath);

            for (Resource resource : resources) {
                filesInputStream.add(resource.getInputStream());
            }

            LOGGER.info("Text files loading was finished successfully");
        } catch (IOException e) {
            LOGGER.error("There is a problem with Text files loading ", e);
        }

        return filesInputStream;
    }

    private int processLine(String word, String line) {
        return toIntExact(Arrays.stream(line.split("\\W+")).filter(s -> s.equals(word)).count());
    }
}
